#include <stdio.h>

int main(){

	int i,n,letras[26]={0};
	char c;


	while(scanf("%d", &n)==1){

		getchar();

		for(i=0; i<n; i++){

			while((c=getchar())!='\n'){

				if((c>='A') && (c<='Z')){

					letras[c-'A']++;

				} else if ((c>='a') && (c<='z')){

					letras[c-'a']++;

				}
			}
		}
	}


	return 0;
}