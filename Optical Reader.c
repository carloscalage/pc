#include <stdio.h>

int main(){

	int n,letras[5],i,j,marca,cont;

	while(scanf("%d", &n)==1 && n!=0){

		for(i = 0; i < n; i++){

			cont=0;
			

			for (j = 0; j < 5; j++){
				
				scanf("%d", &letras[j]);

				if (letras[j]<=127){
					
					marca=j;
					cont++;
				}
			}

			if(cont==1){

				switch(marca){

					case 0:
						printf("A\n");
						break;
					case 1:
						printf("B\n");	
						break;
					case 2:
						printf("C\n");
						break;
					case 3:
						printf("D\n");
						break;
					case 4:
						printf("E\n");
						break;
				}

			} else {
				printf("*\n");
			}
		}
	}

	return 0;
}