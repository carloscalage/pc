#include <stdio.h>
#include <stdlib.h>

void bubbleUp(int list[], int current, int last)
{
	int walker, tempData;

	for(walker = last ; walker > current ; walker--)
		if(list[walker] < list[walker - 1])
		{
			tempData = list[walker - 1];
			list[walker - 1] = list[walker];
			list[walker] =  tempData;
		};

	return;
}

void bubbleSort(int list[], int last)
{
	int current;

	for(current = 0 ; current < last ; current++)
		bubbleUp(list, current, last);

	return;
} 

int main()
{
	int test[5]={47, 29, 11, 8, 24};
	int i;

	bubbleSort(test, 5);

	printf("==== Lista Ordenada ====\n\n");

	for(i = 0 ; i < 5 ; i++)
		printf("%d\n", test[i]);

	return 0;

}