#include <stdio.h>
#include <stdlib.h>

void exchanceSmallest(int list[], int current, int last)
{
	int walker, smallest, tempData;

	smallest = current;
	
	for(walker = current + 1; walker <= last; walker++)
		if(list[walker] < list[smallest])
			smallest = walker;
	
	tempData = list[current];
	list[current] = list[smallest];
	list[smallest] = tempData;

	return;
}

void selectionSort (int list[], int last)
{
	int current;

	for(current = 0; current < last; current++)
		exchanceSmallest(list, current, last);

	return;

};

int main()
{
	int valores[5]={23, 78, 45, 8, 32};
	
	selectionSort(valores, 5);

	for(int i = 0; i < 5; i++)
		printf("%d\n", valores[i]);

	return 0;
}

