#include <stdio.h>

int main(){

    int n, l, i, j, k, aux, count, vagoes[50]={0};

    while(scanf("%d", &n)==1){
        
        for(i=0; i<n; i++){

            count=0;
            scanf("%d", &l);

            for(j=0; j<l; j++){

                scanf("%d", &vagoes[j]);

            }

            for(j=1; j<l; j++){

                for(k=0; k<l-1; k++){
                    
                   if(vagoes[k]>vagoes[k+1]){

                       aux=vagoes[k];
                       vagoes[k]=vagoes[k+1];
                       vagoes[k+1]=aux;
                       count++;

                   }
                }
            }
        
            printf("Optimal train swapping takes %d swaps.\n", count);

        }
    }

    return 0;
}