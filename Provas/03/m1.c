#include <stdio.h>
#include <stdlib.h>

typedef struct Cel {
	int conteudo;
	struct Cel *prox;
} cel;

int main()
{
	int i, b, v;
	FILE *entrada, *saida;
	cel *listas[10], *nova, *p;

	for (i = 0; i < 10; i++) {
		listas[i] = malloc(sizeof(cel));
		listas[i]->prox = NULL;
	}

	entrada = fopen("entrada-m1+m2.txt", "r");

	fscanf(entrada, "%d", &v);

	while (!feof(entrada)) {
		b = (int)(v / 10);

		nova = malloc(sizeof(cel));
		nova->conteudo = v;

		for (p = listas[b]; p->prox != NULL; p = p->prox) {
			if (p->prox->conteudo >= nova->conteudo)
				break;
		}

		nova->prox = p->prox;
		p->prox = nova;

		fscanf(entrada, "%d", &v);
	}
	fclose(entrada);

	saida = fopen("saida.txt", "w");

	for (i = 0; i < 10; i++) {
		for (p = listas[i]->prox; p != NULL; p = p->prox)
			fprintf(saida, "%d ", p->conteudo);
	}
	fclose(saida);

}
