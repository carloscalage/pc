#include <stdio.h>
#include <stdlib.h>

typedef struct cel
{
    char conteudo;
    struct cel *prox;

} celula;

int main()
{
    celula *lista[10], *nova, *p, *fim;
    FILE *in, *out;
    int index;
    char content;

    in = fopen("entrada-m5+m6.txt", "r");

    for(int i = 0 ; i < 10 ; i++)
    {
        lista[i] = malloc(sizeof(celula));
        lista[i]->prox = NULL;
    }

    fscanf(in, "%d %c", &index, &content);

    while(!feof(in))
    {
        nova = malloc(sizeof(celula));
        nova->conteudo = content;

        p = lista[index];

        p->prox = nova;
        nova->prox = NULL;

        fscanf(in, "%d %c", &index, &content);
    }
    

	return 0;
}
