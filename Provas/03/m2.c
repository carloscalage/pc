#include <stdio.h>
#include <stdlib.h>

struct cel {
	int conteudo;
	struct cel *prox;
};

typedef struct cel cel;

int main(int argc, char const *argv[])
{
	FILE *entrada, *saida;
	cel *lista, *nova, *fim, *p, *q;
	int aux, v;

	entrada = fopen("entrada-m1+m2.txt", "r");

	lista = (cel *) malloc(sizeof(struct cel));
	lista->prox = NULL;

	fscanf(entrada, "%d", &v);
	while (!feof(entrada)) {
		/* acha o fim */
		for (fim = lista; fim->prox != NULL; fim = fim->prox) ;
		/* insere no fim */
		nova = (cel *) malloc(sizeof(struct cel));
		nova->conteudo = v;
		nova->prox = fim->prox;
		fim->prox = nova;

		fscanf(entrada, "%d", &v);
	}
	fclose(entrada);

	for (p = lista->prox; p->prox != NULL; p = p->prox) {
		for (q = p->prox; q != NULL; q = q->prox) {
			if (p->conteudo > q->conteudo) {
				aux = p->conteudo;
				p->conteudo = q->conteudo;
				q->conteudo = aux;
			}
		}
	}

	saida = fopen("saida.txt", "w");

	for (p = lista->prox; p != NULL; p = p->prox)
		fprintf(saida, "%d ", p->conteudo);

	fclose(saida);

	return 0;
}
