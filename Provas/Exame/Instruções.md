#Exame M1

##Entrada

- Ler do arquivo entrada.txt os dados e armazenar nas estruturas com listas encadeadas com cabeça.

== Primeira sequência de dados ==

    - Um número inteiro indicando a quantidade de dados a serem lidos para a estrutura
    - Um inteiro indicando o tipo
    - Sequência de dados na ordem da estrutura
    - Rendimento do tipo para a área em metros cúbicos

== Segunda sequência de dados ==

    - Um número inteiro indicando a quantidade de dados a serem lidos para a estrutura
    - Um inteiro indicando o tipo
    - Largura em metros
    - Comprimento em metros
    - Altura em metros


##Saída

- Printar num arquivo saida.txt a soma total dos materiais necessários para a obra

EX:

Quantidades:

    - Cimento: 800.12
    - Areia: 512.00
    - Pedra: 354.35
    - Água: 234.24