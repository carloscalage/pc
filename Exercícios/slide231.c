#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

struct end {
	char nome[100];
	char rua[100];
	char cidade[100];
	char estado[100];
	unsigned long int cep;
} * info;

void cria_lista(void);
void insere(void);
void insere_pelo_nome(void);
void apaga(void);
void apaga_pelo_nome(void);
int busca_pelo_nome(char *nome);
void imprime(void);
void gravar_texto(void);
void gravar_binario(void);
void ler_texto(void);
void ler_binario(void);
void imprime_pelo_nome(void);
int menu(void);
int livre(void);
void ler_string(char palavra[100], int tamanho);

int qtde;

int main()
{
	int escolha;
	cria_lista();
	for (;;) {
		escolha = menu();
		switch (escolha) {
		case 1:
			insere();
			break;
		case 2:
			insere_pelo_nome();
			break;
		case 3:
			apaga();
			break;
		case 4:
			apaga_pelo_nome();
			break;
		case 5:
			imprime();
			break;
		case 6:
			imprime_pelo_nome();
			break;
		case 7:
			ler_texto();
			break;
		case 8:
			ler_binario();
			break;
		case 9:
			gravar_texto();
			break;
		case 10:
			gravar_binario();
			break;
		case 11:
			exit(0);
			break;
		}
	}
	return 0;
}

void cria_lista(void)
{
	qtde = 0;
	info = malloc(sizeof(struct end));

}

int menu(void)
{
	int c = 0;
	do {
		printf("-- MENU:\n");
		printf("\t 1. Insere\n");
		printf("\t 2. Insere verificando o nome\n");
		printf("\t 3. Exclui pela posição\n");
		printf("\t 4. Exclui pelo nome\n");
		printf("\t 5. Imprime todos\n");
		printf("\t 6. Imprime pelo nome\n");
		printf("\t 7. Ler do arquivo texto\n");
		printf("\t 8. Ler do arquivo binário\n");
		printf("\t 9. Gravar no arquivo texto\n");
		printf("\t 10. Gravar no arquivo binário\n");
		printf("\t 11. Sair\n");
		printf("-- Digite sua escolha: ");
		scanf("%d", &c);
	} while (c <= 0 || c > 11);
	getchar();
	return c;
}

void ler_string(char palavra[100], int tamanho)
{
	int i = 0;
	char c;
	c = getchar();
	while ((c != '\n') && (i < tamanho)) {
		palavra[i++] = c;
		c = getchar();
	}
	palavra[i] = '\0';
	if (c != '\n') {
		c = getchar();
		while ((c != '\n') && (c != EOF)) {
			c = getchar();
		}
	}
}

void insere(void)
{
	int posicao;
	posicao = qtde;

	printf("-- Registro %d:\n", posicao);
	printf("\t Nome: ");
	ler_string(info[posicao].nome, 30);
	printf("\t Rua: ");
	ler_string(info[posicao].rua, 40);
	printf("\t Cidade: ");
	ler_string(info[posicao].cidade, 20);
	printf("\t Estado: ");
	ler_string(info[posicao].estado, 2);
	printf("\t CEP: ");
	scanf("%lu", &info[posicao].cep);

	qtde++;
	info = realloc(info, qtde+1);
}

void insere_pelo_nome(void) //adaptar para alocação dinâmica
{
	int posicao;
	char nome[80];

	printf("\t Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);
	if (posicao < 0) {
		posicao = livre();
		if (posicao == -1) {
			printf("\nEstrutura Cheia!!");
			return;
		}
		strcpy(info[posicao].nome, nome);
		printf("\t Rua: ");
		ler_string(info[posicao].rua, 40);
		printf("\t Cidade: ");
		ler_string(info[posicao].cidade, 20);
		printf("\t Estado: ");
		ler_string(info[posicao].estado, 2);
		printf("\t CEP: ");
		scanf("%lu", &info[posicao].cep);
	} else
		printf("-- Nome já existente.\n");
}

int livre(){
	int i;
	for(i=0; i<qtde; i++){

	}
}


void apaga(void)
{
	int posicao;
	int i;
	printf("Número do Registro: ");
	scanf("%d", &posicao);
	if (posicao >= 0 && posicao < qtde) {
		for (i=posicao; i<qtde-1; i++) {
			strcpy(info[i].nome,info[i+1].nome);
			strcpy(info[i].rua,info[i+1].rua);
			strcpy(info[i].cidade,info[i+1].cidade);
			strcpy(info[i].estado,info[i+1].estado);
			info[i].cep=info[i+1].cep;
		}
		qtde--;
		info=realloc(info, qtde+1);
	}
}

void apaga_pelo_nome(void) //adaptar para alocação dinâmica
{
	int posicao;
	char nome[80];

	printf("Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);

	if (posicao >= 0)
		info[posicao].nome[0] = '\0';
	else
		printf("-- Nome não encontrado.\n");
}

int busca_pelo_nome(char *nome)  //adaptar para alocação dinâmica
{
	int i;
	for (i = 0; i < MAX; i++)
		if (strcmp(info[i].nome, nome) == 0)
			return i;
	return -1;
}

void imprime(void)
{
	int i;
	for (i = 0; i < qtde; i++) {
		printf("-- Registro %d:\n", i);
		printf("\t Nome: %s\n", info[i].nome);
		printf("\t Rua: %s\n", info[i].rua);
		printf("\t Cidade: %s\n", info[i].cidade);
		printf("\t Estado: %s\n", info[i].estado);
		printf("\t CEP: %lu\n", info[i].cep);
	}
}

void imprime_pelo_nome(void) //adaptar para alocação dinâmica
{
	int posicao;
	char nome[80];

	printf("Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);

	if (posicao >= 0) {
		printf("-- Registro %d:\n", posicao);
		printf("\t Nome: %s\n", info[posicao].nome);
		printf("\t Rua: %s\n", info[posicao].rua);
		printf("\t Cidade: %s\n", info[posicao].cidade);
		printf("\t Estado: %s\n", info[posicao].estado);
		printf("\t CEP: %lu\n", info[posicao].cep);
	} else
		printf("-- Nome não encontrado.\n");

}

void gravar_texto( void) {
	FILE *arquivo;
	int i;

	arquivo = fopen("arquivo.txt", "w");
	for (i = 0; i < qtde; i++)
		{
			fprintf(arquivo, "%s\n", info[i].nome);
			fprintf(arquivo, "%s\n", info[i].rua);
			fprintf(arquivo, "%s\n", info[i].cidade);
			fprintf(arquivo, "%s\n", info[i].estado);
			fprintf(arquivo, "%lu\n", info[i].cep);
		}
	fclose(arquivo);
}

void gravar_binario( void) {
	FILE *arquivo;
	int i;

	arquivo = fopen("arquivob.txt", "wb");
	for (i = 0; i < qtde; i++)
		{
			fwrite(info[i].nome,sizeof(char),100,arquivo);
			fwrite(info[i].rua,sizeof(char),100,arquivo);
			fwrite(info[i].cidade,sizeof(char),100,arquivo);
			fwrite(info[i].estado,sizeof(char),100,arquivo);
			fwrite(&info[i].cep,sizeof(unsigned long int),1,arquivo);
		}
	fclose(arquivo);
}


void ler_texto( void) {
	FILE *arquivo;
	int i;
	char lixo;

	free(info);
	cria_lista();
	arquivo = fopen("arquivo.txt", "r");

	fscanf(arquivo, "%[^\n]s", info[qtde].nome);
	while( !feof(arquivo)) {
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", info[qtde].rua);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", info[qtde].cidade);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", info[qtde].estado);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%lu", &info[qtde].cep);
		fscanf(arquivo, "%c", &lixo);
	
		qtde++;
		info = realloc(info, qtde+1);

		fscanf(arquivo, "%[^\n]s", info[qtde].nome);
	}

	fclose(arquivo);
}

void ler_binario( void) {
	FILE *arquivo;
	int i;
	char lixo;

	cria_lista();
	arquivo = fopen("arquivob.txt", "rb");

	fread(info[qtde].nome, sizeof(char), 100, arquivo);
	while( !feof(arquivo)) {
		fread(info[qtde].rua, sizeof(char), 100, arquivo);
		fread(info[qtde].cidade, sizeof(char), 100, arquivo);
		fread(info[qtde].estado, sizeof(char), 100, arquivo);
		fread(&info[qtde].cep, sizeof(unsigned long int), 1, arquivo);
	
		qtde++;
		info = realloc(info, qtde+1);
	
		fread(info[qtde].nome, sizeof(char), 100, arquivo);
	}

	fclose(arquivo);
}
