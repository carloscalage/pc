#include<stdio.h>
#include<stdlib.h>

struct cel {
	int conteudo;
	struct cel *prox;
};

typedef struct cel celula;

celula *busca(int x, celula * ini);
celula *cria(void);
void remova(int y, celula * ini);
void remova_end(celula * p);
void insere(int x, celula * p);
void imprime(celula * ini);

celula *busca_fim(celula * ini);


int main()
{
	celula *ini, *pos, *fim;
	ini = cria();
	fim = busca_fim(ini);
	insere(10, fim);
	fim = busca_fim(ini);
	insere(100, fim);
	fim = busca_fim(ini);
	insere(1000, fim);
	imprime(ini);
	pos = busca(100, ini);
	insere(99, pos);
	imprime(ini);
	remova(99, ini);
	imprime(ini);
	pos = busca(100, ini);
	remova_end(pos);
	imprime(ini);
	return 0;
}

celula *cria(void)
{
	celula *start;
	start = (celula *) malloc(sizeof(celula));
	start->prox = NULL;
	return start;
}

void insere(int x, celula * p)
{
	celula *nova;
	nova = (celula *) malloc(sizeof(celula));
	nova->conteudo = x;
	nova->prox = p->prox;
	p->prox = nova;
}

void imprime(celula * ini)
{
	celula *p;
	for (p = ini->prox; p != NULL; p = p->prox)
		printf("%d\n", p->conteudo);
}

celula *busca(int x, celula * ini)
{
	celula *p;
	p = ini->prox;
	while (p != NULL && p->conteudo != x)
		p = p->prox;
	return p;
}

celula *busca_fim(celula * ini)
{
	celula *p;
	p = ini;
	while (p->prox != NULL)
		p = p->prox;
	return p;
}

void remova(int y, celula * ini)
{
	celula *p, *q;
	p = ini;
	q = ini->prox;
	while ((q != NULL) && (q->conteudo != y)) {
		p = q;
		q = q->prox;
	}
	if (q != NULL) {
		p->prox = q->prox;
		free(q);
	}
}

void remova_end(celula * p)
{
	celula *morta;
	morta = p->prox;
	p->prox = morta->prox;
	free(morta);
}
