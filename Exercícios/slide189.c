#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100
struct end {
	char nome[100];
	char rua[100];
	char cidade[100];
	char estado[100];
	unsigned long int cep;
} info[MAX];
void cria_lista(void);
void insere(void);
void insere_pelo_nome(void);
void apaga(void);
void apaga_pelo_nome(void);
int busca_pelo_nome(char *nome);
void imprime(void);
void imprime_pelo_nome(void);
int menu(void);
int livre(void);
void ler_string(char palavra[100], int tamanho);
int main()
{
	int escolha;
	cria_lista();
	for (;;) {
		escolha = menu();
		switch (escolha) {
		case 1:
			insere();
			break;
		case 2:
			insere_pelo_nome();
			break;
		case 3:
			apaga();
			break;
		case 4:
			apaga_pelo_nome();
			break;
		case 5:
			imprime();
			break;
		case 6:
			imprime_pelo_nome();
			break;
		case 7:
			exit(0);
			break;
		}
	}
	return 0;
}

void cria_lista(void)
{
	int i;
	for (i = 0; i < MAX; i++)
		info[i].nome[0] = '\0';
}

int menu(void)
{
	int c = 0;
	do {
		printf("-- MENU:\n");
		printf("\t 1. Insere\n");
		printf("\t 2. Insere verificando o nome\n");
		printf("\t 3. Exclui pela posição\n");
		printf("\t 4. Exclui pelo nome\n");
		printf("\t 5. Imprime todos\n");
		printf("\t 6. Imprime pelo nome\n");
		printf("\t 7. Sair\n");
		printf("-- Digite sua escolha: ");
		scanf("%d", &c);
	} while (c <= 0 || c > 7);
	getchar();
	return c;
}

void ler_string(char palavra[100], int tamanho)
{
	int i = 0;
	char c;
	c = getchar();
	while ((c != '\n') && (i < tamanho)) {
		palavra[i++] = c;
		c = getchar();
	}
	palavra[i] = '\0';
	if (c != '\n') {
		c = getchar();
		while ((c != '\n') && (c != EOF)) {
			c = getchar();
		}
	}
}

void insere(void)
{
	int posicao;
	posicao = livre();
	if (posicao == -1) {
		printf("\nEstrutura Cheia!!");
		return;
	}
	printf("-- Registro %d:\n", posicao);
	printf("\t Nome: ");
	ler_string(info[posicao].nome, 30);
	printf("\t Rua: ");
	ler_string(info[posicao].rua, 40);
	printf("\t Cidade: ");
	ler_string(info[posicao].cidade, 20);
	printf("\t Estado: ");
	ler_string(info[posicao].estado, 2);
	printf("\t CEP: ");
	scanf("%lu", &info[posicao].cep);
}

void insere_pelo_nome(void)
{
	int posicao;
	char nome[80];

	printf("\t Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);
	if (posicao < 0) {
		posicao = livre();
		if (posicao == -1) {
			printf("\nEstrutura Cheia!!");
			return;
		}
		strcpy(info[posicao].nome, nome);
		printf("\t Rua: ");
		ler_string(info[posicao].rua, 40);
		printf("\t Cidade: ");
		ler_string(info[posicao].cidade, 20);
		printf("\t Estado: ");
		ler_string(info[posicao].estado, 2);
		printf("\t CEP: ");
		scanf("%lu", &info[posicao].cep);
	} else
		printf("-- Nome já existente.\n");
}

int livre(void)
{
	int i;
	for (i = 0; info[i].nome[0] && i < MAX; i++) ;
	if (i == MAX)
		return -1;
	return i;
}

void apaga(void)
{
	int posicao;
	printf("Número do Registro: ");
	scanf("%d", &posicao);
	if (posicao >= 0 && posicao < MAX)
		info[posicao].nome[0] = '\0';
}

void apaga_pelo_nome(void)
{
	int posicao;
	char nome[80];

	printf("Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);

	if (posicao >= 0)
		info[posicao].nome[0] = '\0';
	else
		printf("-- Nome não encontrado.\n");
}

int busca_pelo_nome(char *nome)
{
	int i;

	for (i = 0; i < MAX; i++)
		if (strcmp(info[i].nome, nome) == 0)
			return i;
	return -1;
}

void imprime(void)
{
	int i;
	for (i = 0; i < MAX; i++)
		if (info[i].nome[0] != '\0') {
			printf("-- Registro %d:\n", i);
			printf("\t Nome: %s\n", info[i].nome);
			printf("\t Rua: %s\n", info[i].rua);
			printf("\t Cidade: %s\n", info[i].cidade);
			printf("\t Estado: %s\n", info[i].estado);
			printf("\t CEP: %lu\n", info[i].cep);
		}
}

void imprime_pelo_nome(void)
{
	int posicao;
	char nome[80];

	printf("Nome: ");
	ler_string(nome, 80);

	posicao = busca_pelo_nome(nome);

	if (posicao >= 0) {
		printf("-- Registro %d:\n", posicao);
		printf("\t Nome: %s\n", info[posicao].nome);
		printf("\t Rua: %s\n", info[posicao].rua);
		printf("\t Cidade: %s\n", info[posicao].cidade);
		printf("\t Estado: %s\n", info[posicao].estado);
		printf("\t CEP: %lu\n", info[posicao].cep);
	} else
		printf("-- Nome não encontrado.\n");

}
