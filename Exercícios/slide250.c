#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct end {
	char nome[100];
	char rua[100];
	char cidade[100];
	char estado[100];
	unsigned long int cep;
	struct end *prox;
};

typedef struct end end;

void insere(end * p);
void insere_fim(end * p);
// void insere_pelo_nome(void);

// void apaga(void);
void apaga_pelo_nome(end * p);

void imprime(end * p);
// void imprime_pelo_nome(void);

void ler_texto(end * p);
void gravar_texto(end * p);

// void gravar_binario(end * p);
// void ler_binario(void);

// int busca_pelo_nome(char *nome);

int menu(void);
end *cria_lista(void);
void ler_string(char palavra[100], int tamanho);

int qtde;

int main()
{
	int escolha;
	end *info;

	info = cria_lista();

	for (;;) {
		escolha = menu();
		switch (escolha) {
		case 1:
			insere(info);
			break;
		case 2:
			insere_fim(info);
			break;
			// case 3:
			//      apaga();
			//      break;
		case 4:
			apaga_pelo_nome(info);
			break;
		case 5:
			imprime(info);
			break;
			// case 6:
			//      imprime_pelo_nome();
			//      break;
		case 7:
			free(info);
			info = cria_lista();
			ler_texto(info);
			break;
			// case 8:
			//      ler_binario();
			//      break;
		case 9:
			gravar_texto(info);
			break;
			// case 10:
			//      gravar_binario();
			//      break;
		case 11:
			exit(0);
			break;
		}
	}
	return 0;
}

end *cria_lista(void)
{
	end *start;
	start = (end *) malloc(sizeof(end));
	start->prox = NULL;
	return start;
}

int menu(void)
{
	int c = 0;
	do {
		printf("-- MENU:\n");
		printf("\t 1. Insere\n");
		printf("\t 2. Insere no fim\n");
		// printf("\t 3. Exclui pela posição\n");
		printf("\t 4. Exclui pelo nome\n");
		printf("\t 5. Imprime todos\n");
		// printf("\t 6. Imprime pelo nome\n");
		printf("\t 7. Ler do arquivo texto\n");
		// printf("\t 8. Ler do arquivo binário\n");
		printf("\t 9. Gravar no arquivo texto\n");
		// printf("\t 10. Gravar no arquivo binário\n");
		printf("\t 11. Sair\n");
		printf("-- Digite sua escolha: ");
		scanf("%d", &c);
	} while (c <= 0 || c > 11);
	getchar();
	return c;
}

void ler_string(char palavra[100], int tamanho)
{
	int i = 0;
	char c;
	c = getchar();
	while ((c != '\n') && (i < tamanho)) {
		palavra[i++] = c;
		c = getchar();
	}
	palavra[i] = '\0';
	if (c != '\n') {
		c = getchar();
		while ((c != '\n') && (c != EOF)) {
			c = getchar();
		}
	}
}

void insere(end * p)
{
	end *nova;
	nova = (end *) malloc(sizeof(end));

	printf("\t Nome: ");
	ler_string(nova->nome, 100);
	printf("\t Rua: ");
	ler_string(nova->rua, 100);
	printf("\t Cidade: ");
	ler_string(nova->cidade, 100);
	printf("\t Estado: ");
	ler_string(nova->estado, 100);
	printf("\t CEP: ");
	scanf("%lu", &nova->cep);

	nova->prox = p->prox;
	p->prox = nova;

}

void insere_fim(end * p)
{
	end *nova, *fim;
	nova = (end *) malloc(sizeof(end));

	printf("\t Nome: ");
	ler_string(nova->nome, 100);
	printf("\t Rua: ");
	ler_string(nova->rua, 100);
	printf("\t Cidade: ");
	ler_string(nova->cidade, 100);
	printf("\t Estado: ");
	ler_string(nova->estado, 100);
	printf("\t CEP: ");
	scanf("%lu", &nova->cep);

	//acha o fim
	for (fim = p; fim->prox != NULL; fim = fim->prox) ;

	nova->prox = fim->prox;
	fim->prox = nova;

}

// void insere_pelo_nome(void)
// {
//      int posicao;
//      char nome[100];

//      printf("\t Nome: ");
//      ler_string(nome, 100);

//      posicao = busca_pelo_nome(nome);
//      if (posicao < 0) {
//              posicao = qtde;

//              strcpy(info[posicao].nome, nome);
//              printf("\t Rua: ");
//              ler_string(info[posicao].rua, 100);
//              printf("\t Cidade: ");
//              ler_string(info[posicao].cidade, 100);
//              printf("\t Estado: ");
//              ler_string(info[posicao].estado, 100);
//              printf("\t CEP: ");
//              scanf("%lu", &info[posicao].cep);

//              qtde++;
//              info =
//                  (struct end *)realloc(info,
//                                        (qtde + 1) * sizeof(struct end));
//      } else
//              printf("-- Nome já existente.\n");
// }

// void apaga(void)
// {
//      int posicao;
//      int i;
//      printf("Número do Registro: ");
//      scanf("%d", &posicao);
//      if (posicao >= 0 && posicao < qtde) {
//              for (i = posicao; i < qtde - 1; i++) {
//                      strcpy(info[i].nome, info[i + 1].nome);
//                      strcpy(info[i].rua, info[i + 1].rua);
//                      strcpy(info[i].cidade, info[i + 1].cidade);
//                      strcpy(info[i].estado, info[i + 1].estado);
//                      info[i].cep = info[i + 1].cep;
//              }

//              qtde--;
//              info =
//                  (struct end *)realloc(info,
//                                        (qtde + 1) * sizeof(struct end));
//      }
// }

void apaga_pelo_nome(end * ini)
{
	char nome[100];
	end *p, *q;

	printf("Nome: ");
	ler_string(nome, 100);

	p = ini;
	q = ini->prox;
	while ((q != NULL) && (strcmp(q->nome, nome) != 0)) {
		p = q;
		q = q->prox;
	}
	if (q != NULL) {
		p->prox = q->prox;
		free(q);
		printf("\tNome %s removido.\n", nome);
	} else
		printf("\tNome %s inexistente.\n", nome);
}

// int busca_pelo_nome(char *nome)
// {
//      int i;

//      for (i = 0; i < qtde; i++)
//              if (strcmp(info[i].nome, nome) == 0)
//                      return i;
//      return -1;
// }

void imprime(end * ini)
{
	end *p;
	for (p = ini->prox; p != NULL; p = p->prox) {
		printf("\t Nome: %s\n", p->nome);
		printf("\t Rua: %s\n", p->rua);
		printf("\t Cidade: %s\n", p->cidade);
		printf("\t Estado: %s\n", p->estado);
		printf("\t CEP: %lu\n", p->cep);
	}
}

// void imprime_pelo_nome(void)
// {
//      int posicao;
//      char nome[100];
//      printf("Nome: ");
//      ler_string(nome, 100);
//      posicao = busca_pelo_nome(nome);
//      if (posicao >= 0) {
//              printf("-- Registro %d:\n", posicao);
//              printf("\t Nome: %s\n", info[posicao].nome);
//              printf("\t Rua: %s\n", info[posicao].rua);
//              printf("\t Cidade: %s\n", info[posicao].cidade);
//              printf("\t Estado: %s\n", info[posicao].estado);
//              printf("\t CEP: %lu\n", info[posicao].cep);
//      } else
//              printf("-- Nome não encontrado.\n");
// }

void gravar_texto(end * ini)
{
	FILE *arquivo;
	end *p;

	arquivo = fopen("arquivo.txt", "w");
	for (p = ini->prox; p != NULL; p = p->prox) {
		fprintf(arquivo, "%s\n", p->nome);
		fprintf(arquivo, "%s\n", p->rua);
		fprintf(arquivo, "%s\n", p->cidade);
		fprintf(arquivo, "%s\n", p->estado);
		fprintf(arquivo, "%lu\n", p->cep);
	}
	fclose(arquivo);
}

// void gravar_binario(void)
// {
//      FILE *arquivo;
//      int i;

//      arquivo = fopen("arquivob.txt", "wb");
//      for (i = 0; i < qtde; i++) {
//              fwrite(info[i].nome, sizeof(char), 100, arquivo);
//              fwrite(info[i].rua, sizeof(char), 100, arquivo);
//              fwrite(info[i].cidade, sizeof(char), 100, arquivo);
//              fwrite(info[i].estado, sizeof(char), 100, arquivo);
//              fwrite(&info[i].cep, sizeof(unsigned long int), 1, arquivo);
//      }
//      fclose(arquivo);
// }

void ler_texto(end * p)
{
	FILE *arquivo;
	char lixo;

	end *fim, *nova;

	nova = (end *) malloc(sizeof(end));

	arquivo = fopen("arquivo.txt", "r");

	fscanf(arquivo, "%[^\n]s", nova->nome);
	while (!feof(arquivo)) {
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", nova->rua);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", nova->cidade);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%[^\n]s", nova->estado);
		fscanf(arquivo, "%c", &lixo);
		fscanf(arquivo, "%lu", &nova->cep);
		fscanf(arquivo, "%c", &lixo);

		for (fim = p; fim->prox != NULL; fim = fim->prox) ;

		nova->prox = fim->prox;
		fim->prox = nova;

		nova = (end *) malloc(sizeof(end));

		fscanf(arquivo, "%[^\n]s", nova->nome);
	}

	fclose(arquivo);
}

// void ler_binario(void)
// {
//      FILE *arquivo;

//      free(info);
//      cria_lista();
//      arquivo = fopen("arquivob.txt", "rb");

//      fread(info[qtde].nome, sizeof(char), 100, arquivo);
//      while (!feof(arquivo)) {
//              fread(info[qtde].rua, sizeof(char), 100, arquivo);
//              fread(info[qtde].cidade, sizeof(char), 100, arquivo);
//              fread(info[qtde].estado, sizeof(char), 100, arquivo);
//              fread(&info[qtde].cep, sizeof(unsigned long int), 1, arquivo);

//              qtde++;
//              info =
//                  (struct end *)realloc(info,
//                                        (qtde + 1) * sizeof(struct end));

//              fread(info[qtde].nome, sizeof(char), 100, arquivo);
//      }

//      fclose(arquivo);
// }
