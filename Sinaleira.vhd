LIBRARY IEEE;
USE IEEE.std_logic_1164.all;
ENTITY sinaleira is
PORT(
 a: in std_logic;
 b: in std_logic;
 c: in std_logic;
 d: in std_logic;
 ns : out std_logic;
 lo : out std_logic
 );
end sinaleira;
architecture sinal of sinaleira is
begin 
	ns <= (((not c) and (not d) and (a or b)) or (a and b and ((not c) or (not d))));
	lo <= (((not a) and ((not b) or c or d)) or ((not b) and (c or (a and d))) or (a and c and d));


end sinal;