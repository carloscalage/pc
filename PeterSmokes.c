#include <stdio.h>

int main(){

	int n, k, contador;

	while(scanf("%d", &n) == 1){

		do {

			scanf("%d", &k);
		
		} while (k<=1);

		contador = n;
		
		while (n>=k) {

			contador=contador+(n/k);

			n=(n/k)+(n%k);

		}

		printf("%d\n", contador);

	}

	return 0;
}
