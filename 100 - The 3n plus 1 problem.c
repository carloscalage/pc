#include <stdio.h>

int main(){
	
	int i,j,x,aux,valor,acum,maior;

	while(scanf("%d%d", &i,&j)==2){ 
 
		maior=0;

		//VERIFICAR QUAL É O MAIOR DELES

		if(i>j){

			aux=i;
			i=j;
			j=aux;

		} 

		for (x = i; x <= j; x++){
			
			acum=1;
			valor=x;

			while(valor!=1){

				if(valor%2==0){

					valor=valor/2;
					acum++;

				} else {

					valor=3*valor+1;
					acum++;

				}

			}

			if(maior<acum)
				maior=acum;

		}

		printf("%d %d %d\n", i,j,maior);
	}

	return 0;
}


